# Android Weather Application

WeatherApplication which lists weather information for various cities.
Using https://openweathermap.org/current API to fetch weather details for a particular city.
Compatible with android 14 and higher
Followed best UI practises.

Application has four different screens:
1) MainActivity which shows list of cities fetched from database after insertion 
2) SearchActivity which calls API and shows relevant data in a RecylcerView
3) DetailWeatherDataActivity which shows weather data for a particular city selected
4) FavoritesActivity which shows list of city added as favorites from DetailWeatherDataActivity

Also integrated Location API to get current location weather detail based on Latitude and Longitude

Checking for network connectivity using third party library.
-NetConnectivityLib

Using CRUD operations
1) Swipe to delete functionality implemented in MainActivity class
2) Updating using UPDATE query when calling from DetailWeatherDataActivity
3) INSERT query for inserting weather details for a particular city selected in SearchActivity from List
4) Swipe to delete functionality implemented in FavoritesActivity class


Optional Task Execution:

1) Implement ‘More details’ screen (with ability to view more detailed information
about weather data returned from API)
2) Fetching and processing weather data for more locations
3) Refresh the weather data periodically
5) All CRUD operations
6) Tablet mode
