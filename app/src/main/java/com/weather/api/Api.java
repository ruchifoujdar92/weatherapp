package com.weather.api;

import com.weather.model.WeatherData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    /*
     * Created class with static variables to API calls URL
     */
    public static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";
    public static final String API_ID = "25f852836406c080cfe6a5663f9d2e91";

    /*
     * GET method call current weather data for one location based on city name
     */
    @GET("weather?")
    Call<WeatherData> getCityBasedOnName(@Query("q") String cityname,
                                         @Query("appid") String appid);

    /*
     * GET method call current weather data for one location based on latitude and longitude
     */
    @GET("weather?")
    Call<WeatherData> getCityBasedOnLatLon(@Query("lat") String lat,
                                           @Query("lon") String lon,
                                           @Query("appid") String appid);
}
