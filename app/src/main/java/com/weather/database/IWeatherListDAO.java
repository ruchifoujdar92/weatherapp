package com.weather.database;

import com.weather.model.WeatherData;

import java.util.ArrayList;

/*
 * Created interface class
 */
public interface IWeatherListDAO {

    /*
     * @insertData()
     * used to insert data of weather details fetched from api call
     * @parameters: weatherData data model for wrapping data received from api
     * */
    public void insertData(WeatherData weatherData);

    /*
     * @getWeatherList()
     * used to fetch all weather details from database
     * @return ArrayList<WeatherData> list of city with weather details
     * */
    public ArrayList<WeatherData> getWeatherList();

    /*
     * @exitsCityIdLocal()
     * check if there exits a cityID in database
     * based on cityID fetched from api
     * @return boolean true or false
     * */
    public boolean exitsCityIdLocal(String cityIDServer);

    /*
     * @exitsLatLonLocal()
     * check if there exits a latitude and longitude in database
     * based on current location latitude and longitude from location api
     * @parameters: lat and lon
     * @return true or false
     * */
    public boolean exitsLatLonLocal(String lat_lon_current);

    /*
     * @deleteWeather()
     * delete weather data from database based on cityID
     * @parameters: city id from server
     * */
    public void deleteWeather(String cityID);

    /*
     * @deleteFavoritesCity()
     * delete favorites data from database based on cityID
     * @parameters: city id from server
     * */
    public void deleteFavorites(String cityID);

    /*
     * @updateWeatherDetail()
     * update weather details based on new data fetched from api
     * @parameters: weatherData data model for wrapping data received from api
     * */
    public void updateWeatherDetail(WeatherData weatherData);

    /*
     * @getFavorites()
     * used to fetch all favorites city details from database
     * @parameters: flag which is favorites
     * @return ArrayList<WeatherData> list of favorites city with weather details
     * */
    public ArrayList<WeatherData> getFavorites(String flag);

    /*
     * @insertFavoritesData()
     * used to insert data of favorites city details
     * @parameters: weatherData data model for wrapping data received from api
     * */
    public void insertFavoritesData(WeatherData weatherData);

    /*
     * @exitsCityIdFavorites()
     * check if there exits favorites city id in database
     * @parameters: city id from server
     * return true or false
     * */
    public boolean exitsCityIdFavorites(String cityID);
}
