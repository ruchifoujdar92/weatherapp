package com.weather.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.weather.model.Coord;
import com.weather.model.Main;
import com.weather.model.Sys;
import com.weather.model.Weather;
import com.weather.model.WeatherData;
import com.weather.model.Wind;

import java.util.ArrayList;
import java.util.List;

/*
 * Created class for performing CRUD operations
 */
public class WeatherListDAOImpl implements IWeatherListDAO {

    private SQLiteDatabase mSqLiteDatabase;
    private Database mDatabase;

    public WeatherListDAOImpl(Context context) {
        mDatabase = Database.getInstance(context);
    }

    /*
     * @insertData()`
     * used to insert data of weather details fetched from api call
     * @parameters: weatherData data model for wrapping data received from api
     * */
    @Override
    public void insertData(WeatherData weatherData) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(Database.CITY_NAME, weatherData.getName());
        contentValues.put(Database.TEMPERATURE, weatherData.getMain().getTemp());
        contentValues.put(Database.TEMP_MAX, weatherData.getMain().getTempMax());
        contentValues.put(Database.TEMP_MIN, weatherData.getMain().getTempMin());
        contentValues.put(Database.HUMIDITY, weatherData.getMain().getHumidity());
        contentValues.put(Database.PRESSURE, weatherData.getMain().getPressure());
        contentValues.put(Database.FEELS_LIKE, weatherData.getMain().getFeelsLike());
        contentValues.put(Database.CITY_ID_SERVER, weatherData.getId());
        contentValues.put(Database.COUNTRY, weatherData.getSys().getCountry());
        contentValues.put(Database.SUNRISE, weatherData.getSys().getSunrise());
        contentValues.put(Database.SUNSET, weatherData.getSys().getSunset());
        contentValues.put(Database.WIND_SPEED, weatherData.getWind().getSpeed());
        contentValues.put(Database.WEATHER_DESCRIPTION, weatherData.getWeather().get(0).getDescription());
        contentValues.put(Database.LATITUDE, weatherData.getCoord().getLat());
        contentValues.put(Database.LONGITUDE, weatherData.getCoord().getLon());

        open();
        mSqLiteDatabase.insert(Database.TABLE_WEATHER_DATA, null, contentValues);
        close();
    }


    /*
     * @getWeatherList()
     * used to fetch all weather details from database
     * @return ArrayList<WeatherData> list of city with weather details
     * */
    @Override
    public ArrayList<WeatherData> getWeatherList() {
        ArrayList<WeatherData> weatherDTOArrayList = new ArrayList<>();
        List<Weather> list = new ArrayList<>();

        open();
        Cursor cursor = null;
        try {
            cursor = mSqLiteDatabase.rawQuery("select * from " + Database.TABLE_WEATHER_DATA +
                    " ORDER BY " + Database.ID + " DESC", null);
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    do {
                        WeatherData weatherData = new WeatherData();
                        weatherData.setName(cursor.getString(cursor.getColumnIndex(Database.CITY_NAME)));
                        weatherData.setId(cursor.getInt(cursor.getColumnIndex(Database.CITY_ID_SERVER)));

                        Sys sys = new Sys();
                        sys.setCountry(cursor.getString(cursor.getColumnIndex(Database.COUNTRY)));
                        sys.setSunrise(cursor.getString(cursor.getColumnIndex(Database.SUNSET)));
                        sys.setSunset(cursor.getString(cursor.getColumnIndex(Database.SUNRISE)));

                        Coord coord = new Coord();
                        coord.setLat(cursor.getDouble(cursor.getColumnIndex(Database.LATITUDE)));
                        coord.setLon(cursor.getDouble(cursor.getColumnIndex(Database.LONGITUDE)));

                        Wind wind = new Wind();
                        wind.setSpeed(cursor.getString(cursor.getColumnIndex(Database.WIND_SPEED)));

                        Main main = new Main();
                        main.setTemp(cursor.getString(cursor.getColumnIndex(Database.TEMPERATURE)));
                        main.setFeelsLike(cursor.getString(cursor.getColumnIndex(Database.FEELS_LIKE)));
                        main.setHumidity(cursor.getString(cursor.getColumnIndex(Database.HUMIDITY)));
                        main.setPressure(cursor.getString(cursor.getColumnIndex(Database.PRESSURE)));
                        main.setTempMax(cursor.getString(cursor.getColumnIndex(Database.TEMP_MAX)));
                        main.setTempMin(cursor.getString(cursor.getColumnIndex(Database.TEMP_MIN)));

                        Weather weather = new Weather();
                        weather.setDescription(cursor.getString(cursor.getColumnIndex(Database.WEATHER_DESCRIPTION)));
                        try {
                            list.add(weather);
                            weatherData.setWeather(list);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        weatherData.setMain(main);
                        weatherData.setSys(sys);
                        weatherData.setWind(wind);
                        weatherData.setCoord(coord);

                        weatherDTOArrayList.add(weatherData);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();


        } finally {
            if (cursor != null) {
                cursor.close();
            }
            close();
        }
        return weatherDTOArrayList;
    }

    /*
     * @exitsCityIdLocal()
     * check if there exits a cityID in database
     * based on cityID fetched from api
     * @return boolean true or false
     * */
    @Override
    public boolean exitsCityIdLocal(String cityID_server) {
        String cityID_local = null;
        boolean b = false;
        open();
        Cursor cursor = null;
        try {
            cursor = mSqLiteDatabase.rawQuery("select " + Database.CITY_ID_SERVER + " from "
                    + Database.TABLE_WEATHER_DATA + " " +
                    "where " + Database.CITY_ID_SERVER + " =?", new String[]{cityID_server});
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {

                        cityID_local = cursor.getString(cursor.getColumnIndex(Database.CITY_ID_SERVER));
                        if (cityID_server.equals(cityID_local)) {
                            b = true;
                        }
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            close();
        }
        return b;
    }

    /*
     * @exitsLatLonLocal()
     * check if there exits a latitude and longitude in database
     * based on current location latitude and longitude from location api
     * @parameters: lat and lon
     * @return true or false
     * */
    @Override
    public boolean exitsLatLonLocal(String lat_current) {
        String lat = null;
        boolean b = false;
        open();
        Cursor cursor = null;
        try {
            cursor = mSqLiteDatabase.rawQuery("select " + Database.LATITUDE + " from "
                    + Database.TABLE_WEATHER_DATA + " " +
                    "where " + Database.LATITUDE + " =?", new String[]{lat_current});
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {

                        lat = cursor.getString(cursor.getColumnIndex(Database.LATITUDE));
                        if (lat!=null){
                            b=true;
                        }
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            close();
        }
        return b;
    }

    /*
     * @deleteWeather()
     * delete weather data from database based on cityID
     * @parameters: city id from server
     * */
    @Override
    public void deleteWeather(String cityID) {
        open();
        try {
            mSqLiteDatabase.execSQL("DELETE FROM " + Database.TABLE_WEATHER_DATA + " where "
                    + Database.CITY_ID_SERVER + " =?", new String[]{cityID});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /*
     * @deleteFavoritesCity()
     * delete favorites data from database based on cityID
     * @parameters: city id from server
     * */
    @Override
    public void deleteFavorites(String cityID) {
        open();
        try {
            mSqLiteDatabase.execSQL("DELETE FROM " + Database.TABLE_FAVORITES_DATA + " where "
                    + Database.CITY_ID_SERVER + " =?", new String[]{cityID});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /*
     * @updateWeatherDetail()
     * update weather details based on new data fetched from api
     * @parameters: weatherData data model for wrapping data received from api
     * */
    @Override
    public void updateWeatherDetail(WeatherData weatherData) {
        ContentValues values = new ContentValues();
        values.put(Database.COUNTRY, weatherData.getSys().getCountry());
        values.put(Database.TEMPERATURE, weatherData.getMain().getTemp());
        values.put(Database.TEMP_MAX, weatherData.getMain().getTempMax());
        values.put(Database.TEMP_MIN, weatherData.getMain().getTempMin());
        values.put(Database.PRESSURE, weatherData.getMain().getPressure());
        values.put(Database.HUMIDITY, weatherData.getMain().getHumidity());
        values.put(Database.WEATHER_DESCRIPTION, weatherData.getWeather().get(0).getDescription());
        values.put(Database.SUNSET, weatherData.getSys().getSunset());
        values.put(Database.SUNRISE, weatherData.getSys().getSunrise());
        values.put(Database.WIND_SPEED, weatherData.getWind().getSpeed());
        values.put(Database.FEELS_LIKE, weatherData.getMain().getFeelsLike());
        values.put(Database.CITY_NAME, weatherData.getName());
        open();
        try {
            mSqLiteDatabase.update(Database.TABLE_WEATHER_DATA, values,
                    Database.CITY_ID_SERVER + " =?", new String[]{weatherData.getId() + ""});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /*
     * @getFavorites()
     * used to fetch all favorites city details from database
     * @parameters: flag which is favorites
     * @return ArrayList<WeatherData> list of favorites city with weather details
     * */
    @Override
    public ArrayList<WeatherData> getFavorites(String flag) {
        ArrayList<WeatherData> weatherDTOArrayList = new ArrayList<>();
        List<Weather> list = new ArrayList<>();

        open();
        Cursor cursor = null;
        try {
            cursor = mSqLiteDatabase.rawQuery("select * from " + Database.TABLE_FAVORITES_DATA +
                            " where " + Database.FAVORITES + " =?" + " ORDER BY " +   Database.ID + " DESC ",
                    new String[]{flag});
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    do {
                        WeatherData weatherData = new WeatherData();
                        weatherData.setName(cursor.getString(cursor.getColumnIndex(Database.CITY_NAME)));
                        weatherData.setId(cursor.getInt(cursor.getColumnIndex(Database.CITY_ID_SERVER)));

                        Sys sys = new Sys();
                        sys.setCountry(cursor.getString(cursor.getColumnIndex(Database.COUNTRY)));
                        sys.setSunrise(cursor.getString(cursor.getColumnIndex(Database.SUNSET)));
                        sys.setSunset(cursor.getString(cursor.getColumnIndex(Database.SUNRISE)));

                        Wind wind = new Wind();
                        wind.setSpeed(cursor.getString(cursor.getColumnIndex(Database.WIND_SPEED)));

                        Main main = new Main();
                        main.setTemp(cursor.getString(cursor.getColumnIndex(Database.TEMPERATURE)));
                        main.setFeelsLike(cursor.getString(cursor.getColumnIndex(Database.FEELS_LIKE)));
                        main.setHumidity(cursor.getString(cursor.getColumnIndex(Database.HUMIDITY)));
                        main.setPressure(cursor.getString(cursor.getColumnIndex(Database.PRESSURE)));
                        main.setTempMax(cursor.getString(cursor.getColumnIndex(Database.TEMP_MAX)));
                        main.setTempMin(cursor.getString(cursor.getColumnIndex(Database.TEMP_MIN)));

                        Weather weather = new Weather();
                        weather.setDescription(cursor.getString(cursor.getColumnIndex(Database.WEATHER_DESCRIPTION)));
                        try {
                            list.add(weather);
                            weatherData.setWeather(list);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        weatherData.setMain(main);
                        weatherData.setSys(sys);
                        weatherData.setWind(wind);
                        weatherDTOArrayList.add(weatherData);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();


        } finally {
            if (cursor != null) {
                cursor.close();
            }
            close();
        }
        return weatherDTOArrayList;
    }

    /*
     * @insertFavoritesData()
     * used to insert data of favorites city details
     * @parameters: weatherData data model for wrapping data received from api
     * */
    @Override
    public void insertFavoritesData(WeatherData weatherData) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(Database.CITY_NAME, weatherData.getName());
        contentValues.put(Database.TEMPERATURE, weatherData.getMain().getTemp());
        contentValues.put(Database.TEMP_MAX, weatherData.getMain().getTempMax());
        contentValues.put(Database.TEMP_MIN, weatherData.getMain().getTempMin());
        contentValues.put(Database.HUMIDITY, weatherData.getMain().getHumidity());
        contentValues.put(Database.PRESSURE, weatherData.getMain().getPressure());
        contentValues.put(Database.FEELS_LIKE, weatherData.getMain().getFeelsLike());
        contentValues.put(Database.CITY_ID_SERVER, weatherData.getId());
        contentValues.put(Database.COUNTRY, weatherData.getSys().getCountry());
        contentValues.put(Database.SUNRISE, weatherData.getSys().getSunrise());
        contentValues.put(Database.SUNSET, weatherData.getSys().getSunset());
        contentValues.put(Database.WIND_SPEED, weatherData.getWind().getSpeed());
        contentValues.put(Database.WEATHER_DESCRIPTION, weatherData.getWeather().get(0).getDescription());
        contentValues.put(Database.FAVORITES, weatherData.getFavorites());

        open();
        mSqLiteDatabase.insert(Database.TABLE_FAVORITES_DATA, null, contentValues);
        close();
    }

    /*
     * @exitsCityIdFavorites()
     * check if there exits favorites city id in database
     * @parameters: city id from server
     * return true or false
     * */
    @Override
    public boolean exitsCityIdFavorites(String cityID) {
        String cityID_local = null;
        boolean b = false;
        open();
        Cursor cursor = null;
        try {
            cursor = mSqLiteDatabase.rawQuery("select " + Database.CITY_ID_SERVER + " from "
                    + Database.TABLE_FAVORITES_DATA + " " +
                    "where " + Database.CITY_ID_SERVER + " =?", new String[]{cityID});
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {

                        cityID_local = cursor.getString(cursor.getColumnIndex(Database.CITY_ID_SERVER));
                        if (cityID_local != null) {
                            b = true;
                        }

                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            close();
        }
        return b;
    }

    private void open() {
        try {
            mSqLiteDatabase = mDatabase.getReadableDatabase();
        } catch (Exception openException) {
            openException.printStackTrace();
        }

    }

    private void close() {
        try {
            if (mSqLiteDatabase != null) {
                mSqLiteDatabase.close();
            }
        } catch (Exception closeException) {
            closeException.printStackTrace();
        }
    }
}
