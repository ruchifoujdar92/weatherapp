package com.weather.favorites;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.weather.R;
import com.weather.database.IWeatherListDAO;
import com.weather.database.WeatherListDAOImpl;
import com.weather.model.WeatherData;
import com.weather.util.SwipeToDeleteCallback;

import java.util.ArrayList;

public class FavoritesActivity extends AppCompatActivity {

    private RecyclerView mRecyclerFavorites;
    private FavoritesCustomAdapter mFavoritesCustomAdapter;
    private IWeatherListDAO mIweatherListDAO;
    private ArrayList<WeatherData> mWeatherDataArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        getSupportActionBar().setTitle("Favorites");

        mRecyclerFavorites = (RecyclerView) findViewById(R.id.favorites_list);
        mRecyclerFavorites.setLayoutManager(new LinearLayoutManager(this));
        mWeatherDataArrayList = new ArrayList<>();
        mIweatherListDAO = new WeatherListDAOImpl(this);

        mWeatherDataArrayList = mIweatherListDAO.getFavorites("favorites");

        if (mWeatherDataArrayList != null) {
            mFavoritesCustomAdapter = new FavoritesCustomAdapter(mWeatherDataArrayList);
            mRecyclerFavorites.setAdapter(mFavoritesCustomAdapter);
            enableSwipeToDelete();
        }
    }

    //swipe to delete functionality
    private void enableSwipeToDelete() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                final int position = viewHolder.getAdapterPosition();
                String cityid = mWeatherDataArrayList.get(position).getId() + "";
                mFavoritesCustomAdapter.removeItem(position);
                mIweatherListDAO.deleteFavorites(cityid);
            }
        };
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(mRecyclerFavorites);
    }
}
