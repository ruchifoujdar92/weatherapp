package com.weather.favorites;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.weather.R;
import com.weather.model.WeatherData;

import java.util.ArrayList;

/*
 * Created Custom Adapter for Recyclerview
 */
public class FavoritesCustomAdapter extends RecyclerView.Adapter<FavoritesCustomAdapter.ViewHolder> {

    private ArrayList<WeatherData> mWeatherArrayList;

    public FavoritesCustomAdapter(ArrayList<WeatherData> mWeatherArrayList) {
        // TODO Auto-generated constructor stub
        this.mWeatherArrayList = mWeatherArrayList;
    }

    @NonNull
    @Override
    public FavoritesCustomAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorites_list_adapter, null);
        final ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WeatherData weatherData = mWeatherArrayList.get(position);
        holder.textView_cityName.setText(weatherData.getName());
        holder.textView_degree.setText(weatherData.getMain().getTemp());
    }

    @Override
    public int getItemCount() {
        return mWeatherArrayList.size();
    }

    public void removeItem(int position) {
        mWeatherArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mWeatherArrayList.size());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_cityName;
        private TextView textView_degree;

        public ViewHolder(View view) {
            super(view);
            this.textView_cityName = (TextView) view.findViewById(R.id.textView_cityName);
            this.textView_degree = (TextView) view.findViewById(R.id.textView_degree);
        }
    }


}
