package com.weather.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * Retrofit generated class with API call parameters
 */
public class Weather {

    @SerializedName("description")
    @Expose
    private String description;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
