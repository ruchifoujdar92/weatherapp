package com.weather.search;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Toast;

import com.weather.database.IWeatherListDAO;
import com.weather.database.WeatherListDAOImpl;
import com.weather.model.WeatherData;
import com.weather.network.NetworkConnectivity;
import com.weather.R;
import com.weather.service.IWeatherService;
import com.weather.service.WeatherServiceImpl;
import com.weather.util.MainActivity;
import com.weather.util.RecyclerItemClickListener;

import java.util.ArrayList;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/*
 * Created class SearchActivity for showing
 * list of city in a RecyclerView
 */
public class SearchActivity extends AppCompatActivity {

    private SearchView mSearch;
    private RecyclerView mSearchResults;
    private SearchResultAdapter mSearchResultAdapter;
    private IWeatherListDAO mIWeatherListDAO;
    private ArrayList<WeatherData> mWeatherDTOArrayList;
    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        pDialog = new ProgressDialog(SearchActivity.this);
        mSearch = findViewById(R.id.searchView1);
        mSearch.setQueryHint("Start typing to search...");
        mSearchResults = findViewById(R.id.recyclerview_search);
        mSearchResults.setLayoutManager(new LinearLayoutManager(SearchActivity.this));

        mSearch.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
            }
        });
        mIWeatherListDAO = new WeatherListDAOImpl(SearchActivity.this);
        mSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub

                return false;
            }

            @Override
            public boolean onQueryTextChange(String cityName) {
                //checking length of text and calling api only when length is greater than 3
                if (cityName.length() > 3) {
                    mSearchResults.setVisibility(VISIBLE);
                    IWeatherService weatherService = new WeatherServiceImpl();
                    NetworkConnectivity networkConnectivity = new NetworkConnectivity(SearchActivity.this);
                    if (networkConnectivity.isConnected()) {
                        pDialog.setMessage("Loading...");
                        pDialog.show();
                        weatherService.getWeatherList("city_name", " ", " ", cityName,
                                new WeatherServiceImpl.DataCallback() {
                            @Override
                            public void onSuccess(WeatherData weatherData) {
                                pDialog.dismiss();
                                if (weatherData != null) {
                                    mWeatherDTOArrayList = new ArrayList<>();
                                    mWeatherDTOArrayList.add(weatherData);
                                    mSearchResultAdapter = new SearchResultAdapter(mWeatherDTOArrayList);
                                    mSearchResults.setAdapter(mSearchResultAdapter);
                                }
                            }
                                    @Override
                                    public void onFailure(String response) {
                                        if (response!=null){
                                            pDialog.dismiss();
                                            Toast.makeText(SearchActivity.this,response,Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    } else {
                        networkConnectivity.connectivity_checker();
                    }

                } else {
                    mSearchResults.setVisibility(INVISIBLE);
                }
                return false;
            }

        });
        mSearchResults.addOnItemTouchListener(new RecyclerItemClickListener(SearchActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                WeatherData weatherData = mWeatherDTOArrayList.get(position);
                String cityId_server = weatherData.getId() + "";
                IWeatherListDAO iWeatherListDAO = new WeatherListDAOImpl(SearchActivity.this);
                boolean exitsCityID_local = iWeatherListDAO.exitsCityIdLocal(cityId_server);
                //if id exits do not insert
                if (exitsCityID_local) {
                    //do not insert
                    Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    mIWeatherListDAO.insertData(weatherData);
                    Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
