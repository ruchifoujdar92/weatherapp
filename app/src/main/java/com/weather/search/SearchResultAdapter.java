package com.weather.search;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.weather.R;
import com.weather.model.WeatherData;

import java.util.ArrayList;

/*
 * Created Custom Adapter for RecyclerView
 */
public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {

    private ArrayList<WeatherData> mWeatherArrayList;

    public SearchResultAdapter(ArrayList<WeatherData> mWeatherArrayList) {
        // TODO Auto-generated constructor stub
        this.mWeatherArrayList = mWeatherArrayList;
    }

    @NonNull
    @Override
    public SearchResultAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.serach_result_adpater, null);
        final ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WeatherData weatherData = mWeatherArrayList.get(position);
        holder.textView_cityName.setText(weatherData.getName() + "," + weatherData.getSys().getCountry());

    }

    @Override
    public int getItemCount() {
        return mWeatherArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_cityName;

        public ViewHolder(View view) {
            super(view);
            this.textView_cityName = (TextView) view.findViewById(R.id.textView_cityName);
        }
    }
}
