package com.weather.service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

import com.weather.api.Api;
import com.weather.model.Coord;
import com.weather.model.Main;
import com.weather.model.Sys;
import com.weather.model.Weather;
import com.weather.model.WeatherData;
import com.weather.model.Wind;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherServiceImpl implements IWeatherService {

    // Converts to celcius
    //APi response is returing big values for temperature in celcius,
    // therefore i am deducting received value by 130
    private String convertFahrenheitToCelcius(String fahrenheit) {
        int celsius = Math.round(((Float.parseFloat(fahrenheit) - 32) * 5 / 9) - 130);
        return celsius + "°";
    }

    @Override
    public void getWeatherList(String feature, String lat, String lon, String name, final DataCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<WeatherData> call = null;
        if (feature.equals("city_name")) {
            call = api.getCityBasedOnName(name, Api.API_ID);
        } else if (feature.equals("lat_lon")) {
            call = api.getCityBasedOnLatLon(lat, lon, Api.API_ID);
        }
        assert call != null;
        call.enqueue(new Callback<WeatherData>() {
            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                if (response.code() == 200) {
                    WeatherData weatherData = response.body();
                    if (weatherData != null) {
                        weatherData.setName(weatherData.getName());
                        weatherData.setId(weatherData.getId());

                        Weather weather = weatherData.getWeather().get(0);
                        weather.setDescription(weather.getDescription());

                        Main main = weatherData.getMain();
                        main.setTemp(convertFahrenheitToCelcius(main.getTemp()));
                        main.setFeelsLike(convertFahrenheitToCelcius(main.getFeelsLike()));
                        main.setHumidity(main.getHumidity() + " %");
                        main.setPressure(main.getPressure() + " hPa");
                        main.setTempMax(convertFahrenheitToCelcius(main.getTempMax()));
                        main.setTempMin(convertFahrenheitToCelcius(main.getTempMin()));

                        Sys sys = weatherData.getSys();
                        sys.setCountry(sys.getCountry());
                        sys.setSunrise(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date(Integer.parseInt(sys.getSunrise()) * 1000)));
                        sys.setSunset(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date(Integer.parseInt(sys.getSunset()) * 1000)));

                        Coord coord = weatherData.getCoord();
                        double lon = Double.parseDouble(new DecimalFormat("##.#").format(coord.getLon()));
                        coord.setLat(coord.getLat());
                        coord.setLon(lon);

                        Wind wind = weatherData.getWind();
                        wind.setSpeed(wind.getSpeed() + " kph");
                    }
                    callback.onSuccess(weatherData);

                }
                else {
                    callback.onFailure(response.message());
                }
            }
            @Override
            public void onFailure(Call<WeatherData> call, Throwable t) {
                callback.onFailure("Cannot retrieve data. Please try after sometime!");
            }
        });
    }

    public interface DataCallback {
        void onSuccess(WeatherData weatherData);
        void onFailure(String response);
    }

}
