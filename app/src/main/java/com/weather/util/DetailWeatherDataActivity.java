package com.weather.util;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.weather.database.IWeatherListDAO;
import com.weather.database.WeatherListDAOImpl;
import com.weather.model.Main;
import com.weather.model.Sys;
import com.weather.model.Weather;
import com.weather.model.WeatherData;
import com.weather.model.Wind;
import com.weather.network.NetworkConnectivity;
import com.weather.R;
import com.weather.search.SearchActivity;
import com.weather.service.IWeatherService;
import com.weather.service.WeatherServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class DetailWeatherDataActivity extends AppCompatActivity {

    private TextView mCityName_tv, mWeatherDescription_tv,
            mTemp_tv, mTemp_min_tv, mTemp_max_tv,
            mSunrise_tv, mSunset_tv, mWind_tv,
            mPressure_tv, mHumidity_tv, mFeels_like_tv;
    private ImageView mFavorites_added, mFavorites_not_added;
    private IWeatherListDAO mIWeatherListDAO;
    private RelativeLayout mRelativeFavorites;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_weather_data);

        mCityName_tv = findViewById(R.id.cityName_tv);
        mWeatherDescription_tv = findViewById(R.id.weatherDescription_tv);
        mTemp_tv = findViewById(R.id.temp_tv);
        mTemp_min_tv = findViewById(R.id.temp_min_tv);
        mTemp_max_tv = findViewById(R.id.temp_max_tv);
        mSunrise_tv = findViewById(R.id.sunrise_tv);
        mSunset_tv = findViewById(R.id.sunset_tv);
        mWind_tv = findViewById(R.id.wind_tv);
        mPressure_tv = findViewById(R.id.pressure_tv);
        mHumidity_tv = findViewById(R.id.humidity_tv);
        mFeels_like_tv = findViewById(R.id.feels_like_tv);
        mFavorites_added = findViewById(R.id.imgeView_added_favorites);
        mFavorites_not_added = findViewById(R.id.imgeView_not_added_favorites);
        mRelativeFavorites = findViewById(R.id.relative_favorites);
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mIWeatherListDAO = new WeatherListDAOImpl(DetailWeatherDataActivity.this);

        pDialog = new ProgressDialog(DetailWeatherDataActivity.this);

        Intent intent = getIntent();
        final String city_name = intent.getStringExtra("city_name");
        final int city_id = intent.getIntExtra("city_id", 0);
        final String country = intent.getStringExtra("country");
        final String humidity = intent.getStringExtra("humidity");
        final String pressure = intent.getStringExtra("pressure");
        final String temp_max = intent.getStringExtra("temp_max");
        final String temp_min = intent.getStringExtra("temp_min");
        final String temp = intent.getStringExtra("temp");
        final String feels_like = intent.getStringExtra("feels_like");
        final String sunrise = intent.getStringExtra("sunrise");
        final String sunset = intent.getStringExtra("sunset");
        final String wind_speed = intent.getStringExtra("wind");
        final String weather_description = intent.getStringExtra("weather_description");

        boolean exitsCityID_local = mIWeatherListDAO.exitsCityIdFavorites(city_id + "");
        //if id exits do not insert
        if (exitsCityID_local) {
            mFavorites_added.setVisibility(View.VISIBLE);
            mFavorites_not_added.setVisibility(View.INVISIBLE);
        } else {
            mFavorites_not_added.setVisibility(View.VISIBLE);
            mFavorites_added.setVisibility(View.INVISIBLE);

        }
        mCityName_tv.setText(city_name + ", " + country);
        IWeatherService weatherService = new WeatherServiceImpl();

        //check for network connection available or not
        NetworkConnectivity networkConnectivity = new NetworkConnectivity(DetailWeatherDataActivity.this);
        if (networkConnectivity.isConnected()) {
            pDialog.setMessage("Loading...");
            pDialog.show();
            weatherService.getWeatherList("city_name", " ", " ", city_name, new WeatherServiceImpl.DataCallback() {
                @Override
                public void onSuccess(WeatherData weatherData) {
                    pDialog.dismiss();
                    if (weatherData != null) {
                        mWeatherDescription_tv.setText(weatherData.getWeather().get(0).getDescription().toUpperCase());
                        mTemp_tv.setText(weatherData.getMain().getTemp() + "");
                        mTemp_max_tv.setText("Max Temperature:" + weatherData.getMain().getTempMax());
                        mTemp_min_tv.setText("Min Temperature:" + weatherData.getMain().getTempMin());
                        mSunrise_tv.setText(weatherData.getSys().getSunrise());
                        mSunset_tv.setText(weatherData.getSys().getSunset());
                        mWind_tv.setText(weatherData.getWind().getSpeed() + "");
                        mHumidity_tv.setText(weatherData.getMain().getHumidity());
                        mPressure_tv.setText(weatherData.getMain().getPressure());
                        mFeels_like_tv.setText(weatherData.getMain().getFeelsLike());
                        mIWeatherListDAO.updateWeatherDetail(weatherData);
                    }
                }

                @Override
                public void onFailure(String response) {
                    if (response!=null){
                        pDialog.dismiss();
                        Toast.makeText(DetailWeatherDataActivity.this,response,Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            networkConnectivity.connectivity_checker();
            if (weather_description == null) throw new AssertionError();
            mWeatherDescription_tv.setText(weather_description.toUpperCase());
            mTemp_tv.setText(temp + "");
            mTemp_max_tv.setText("Max Temperature:" + temp_max);
            mTemp_min_tv.setText("Min Temperature:" + temp_min);
            mSunrise_tv.setText(sunrise);
            mSunset_tv.setText(sunset);
            mWind_tv.setText(wind_speed);
            mHumidity_tv.setText(humidity);
            mPressure_tv.setText(pressure);
            mFeels_like_tv.setText(feels_like + "");
        }
        mRelativeFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<Weather> list = new ArrayList<>();
                WeatherData weatherData = new WeatherData();
                weatherData.setName(city_name);
                weatherData.setFavorites("favorites");
                weatherData.setId(city_id);

                Weather weather = new Weather();
                weather.setDescription(weather_description);

                Main main = new Main();
                main.setTemp(temp);
                main.setFeelsLike(feels_like);
                main.setHumidity(humidity);
                main.setPressure(pressure);
                main.setTempMax(temp_max);
                main.setTempMin(temp_min);

                Sys sys = new Sys();
                sys.setCountry(country);
                sys.setSunrise(sunrise);
                sys.setSunset(sunset);

                Wind wind = new Wind();
                wind.setSpeed(wind_speed);
                try {
                    list.add(weather);
                    weatherData.setWeather(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                weatherData.setMain(main);
                weatherData.setSys(sys);
                weatherData.setWind(wind);
                //if id exits do not insert
                boolean exitsCityID_local = mIWeatherListDAO.exitsCityIdFavorites(city_id + "");
                if (exitsCityID_local) {
                    //do not insert
                    Toast.makeText(DetailWeatherDataActivity.this, "Already added to favorites!", Toast.LENGTH_SHORT).show();
                } else {
                    mIWeatherListDAO.insertFavoritesData(weatherData);
                    mFavorites_added.setVisibility(View.VISIBLE);
                    mFavorites_not_added.setVisibility(View.INVISIBLE);
                    Toast.makeText(DetailWeatherDataActivity.this, "Added to favorites!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
