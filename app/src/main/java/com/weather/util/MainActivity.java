package com.weather.util;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.weather.database.IWeatherListDAO;
import com.weather.database.WeatherListDAOImpl;
import com.weather.favorites.FavoritesActivity;
import com.weather.model.WeatherData;
import com.weather.network.NetworkConnectivity;
import com.weather.R;
import com.weather.search.SearchActivity;
import com.weather.service.IWeatherService;
import com.weather.service.WeatherServiceImpl;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerWeather;
    private WeatherCustomAdapter weatherCustomAdapter;
    private IWeatherListDAO miWeatherListDAO;
    private ArrayList<WeatherData> weatherDTOArrayList;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private LocationManager mLocationManager;
    public static final int LOCATION_UPDATE_MIN_DISTANCE = 10;
    public static final int LOCATION_UPDATE_MIN_TIME = 5000;
    private ProgressDialog pDialog;

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            try {
                getLocation(location);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pDialog = new ProgressDialog(MainActivity.this);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mRecyclerWeather = findViewById(R.id.weather_list);
        mRecyclerWeather.setLayoutManager(new LinearLayoutManager(this));
        weatherDTOArrayList = new ArrayList<>();
        miWeatherListDAO = new WeatherListDAOImpl(this);
        weatherDTOArrayList = miWeatherListDAO.getWeatherList();
        checkPermission();
        if (weatherDTOArrayList != null) {
            weatherCustomAdapter = new WeatherCustomAdapter(MainActivity.this, weatherDTOArrayList);
            mRecyclerWeather.setAdapter(weatherCustomAdapter);
            enableSwipeToDelete();

            mRecyclerWeather.addOnItemTouchListener(new RecyclerItemClickListener(MainActivity.this,
                    new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Intent intent = new Intent(MainActivity.this, DetailWeatherDataActivity.class);
                            if (weatherDTOArrayList != null) {
                                intent.putExtra("city_name", weatherDTOArrayList.get(position).getName());
                                intent.putExtra("city_id", weatherDTOArrayList.get(position).getId());
                                intent.putExtra("country", weatherDTOArrayList.get(position).getSys().getCountry());
                                intent.putExtra("humidity", weatherDTOArrayList.get(position).getMain().getHumidity());
                                intent.putExtra("pressure", weatherDTOArrayList.get(position).getMain().getPressure());
                                intent.putExtra("temp_max", weatherDTOArrayList.get(position).getMain().getTempMax());
                                intent.putExtra("temp_min", weatherDTOArrayList.get(position).getMain().getTempMin());
                                intent.putExtra("temp", weatherDTOArrayList.get(position).getMain().getTemp());
                                intent.putExtra("feels_like", weatherDTOArrayList.get(position).getMain().getFeelsLike());
                                intent.putExtra("sunrise", weatherDTOArrayList.get(position).getSys().getSunrise());
                                intent.putExtra("sunset", weatherDTOArrayList.get(position).getSys().getSunset());
                                intent.putExtra("wind", weatherDTOArrayList.get(position).getWind().getSpeed());
                                intent.putExtra("weather_description", weatherDTOArrayList.get(position).getWeather().get(0).getDescription());
                                startActivity(intent);
                                finish();

                            }
                        }
                    }));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                // User chose the "Settings" item, show the app settings UI...
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.favorites:
                intent = new Intent(MainActivity.this, FavoritesActivity.class);
                startActivity(intent);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    //swipe to delete functionality
    private void enableSwipeToDelete() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                final int position = viewHolder.getAdapterPosition();
                String cityid = weatherDTOArrayList.get(position).getId() + "";
                weatherCustomAdapter.removeItem(position);
                miWeatherListDAO.deleteWeather(cityid);
            }
        };
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(mRecyclerWeather);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        getCurrentLocation();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            getCurrentLocation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getLocation(Location location) {
        Log.d(getPackageName(), "LocationProvider:" + location.toString());

        Log.v(getPackageName(), String.format("getCurrentLocation(%f, %f)", location.getLatitude(),
                location.getLongitude()));
        String lat = location.getLatitude() + "";
        String lon = location.getLongitude() + "";
        double value = location.getLatitude();
        value = Double.parseDouble(new DecimalFormat("##.#").format(value));
        boolean exits_lat = miWeatherListDAO.exitsLatLonLocal(value + "");
        //check if latitude exits in database or not
        if (exits_lat) {
            if (weatherDTOArrayList != null) {
                weatherCustomAdapter = new WeatherCustomAdapter(MainActivity.this, weatherDTOArrayList);
                mRecyclerWeather.setAdapter(weatherCustomAdapter);
                enableSwipeToDelete();
            }
        } else {
            IWeatherService weatherService = new WeatherServiceImpl();
            NetworkConnectivity networkConnectivity = new NetworkConnectivity(MainActivity.this);
            if (networkConnectivity.isConnected()) {
                pDialog.setMessage("Loading...");
                pDialog.show();
                weatherService.getWeatherList("lat_lon", lat, lon, " ",
                        new WeatherServiceImpl.DataCallback() {
                            @Override
                            public void onSuccess(WeatherData weatherData) {
                                pDialog.dismiss();
                                if (weatherData != null) {
                                    weatherDTOArrayList = new ArrayList<>();
                                    weatherDTOArrayList.add(weatherData);
                                    boolean exitsCityID_local = miWeatherListDAO.exitsCityIdLocal(weatherData.getId() + "");
                                    //if id exits do not insert
                                    if (!exitsCityID_local) {
                                        miWeatherListDAO.insertData(weatherData);
                                    }
                                    weatherDTOArrayList = miWeatherListDAO.getWeatherList();
                                    weatherCustomAdapter = new WeatherCustomAdapter(MainActivity.this, weatherDTOArrayList);
                                    mRecyclerWeather.setAdapter(weatherCustomAdapter);
                                    enableSwipeToDelete();
                                }
                            }

                            @Override
                            public void onFailure(String response) {
                                if (response!=null){
                                    pDialog.dismiss();
                                    Toast.makeText(MainActivity.this,response,Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            } else {
                networkConnectivity.connectivity_checker();
            }
        }
    }

    // check if user has granted location permission
    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST_CODE);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST_CODE);
            }
        } else {
            try {
                getCurrentLocation();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //if location permission is granted then get location(lat and lon)
    public void getCurrentLocation(){
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Location location;
        if (isNetworkEnabled) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            } else {
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
                location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    getLocation(location);
                    mLocationManager.removeUpdates(mLocationListener);
                }
                else {
                    Toast.makeText(MainActivity.this,"Unable to fetch location, please check if GPS is enabled",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
